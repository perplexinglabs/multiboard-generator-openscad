$fa = 1;
$fs = 4;

rows = 5;
cols = 5;
left_edge = true;
right_edge = true;
top_edge = true;
bot_edge = true;


function bint(v) = (v ? 1 : 0);

translate([-(cols - 1) * 25 / 2, 0, -(rows - 1) * 25 / 2])
rotate([0, 0, -90])
color("tomato")
for (r = [0 : rows - 1]) {
    for (c = [0 : cols - 1]) {
        translate([0, c * 25, r * 25])
        rotate([0, 90, 0])
        import("Tile Multihole Component.stl", convexity=3);
    }
}

r_start = 1 - bint(bot_edge);
r_end = rows - 1 + bint(top_edge);
c_start = 0 - bint(left_edge);
c_end = cols - 2 + bint(right_edge);

translate([-(cols - 1) * 25 / 2, 0, -(rows - 1) * 25 / 2])
rotate([0, 0, -90])
color("tomato")
for (r = [r_start : r_end]) {
    for (c = [c_start : c_end]) {
        translate([0, c * 25, r * 25])
        rotate([0, 90, 0])
        import("Tile Pegboard Hole Component.stl", convexity=3);
    }
};


